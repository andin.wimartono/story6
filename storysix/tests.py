from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Activity, Member

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.activity_url = reverse("storysix:activity")
        self.test_activity = Activity.objects.create(
            activity = "belajar"
        )
        self.test_member = Member.objects.create(
            member = "andin",
            aktivitas = self.test_activity
        )
        self.test_delete_url = reverse("storysix:activityDelete",args=[self.test_activity.id])
        self.test_member_delete_url = reverse("storysix:memberDelete",args=[self.test_member.id])

    def test_activity_GET(self):
        response = self.client.get(self.activity_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "index.html")

    def test_activity_POST(self):
        response = self.client.post(self.activity_url, {
            "activity" : "makan"
        }, follow=True)
        self.assertContains(response, "makan")

    def test_member_POST(self):
        response = self.client.post(self.activity_url, {
            "member" : "indi",
            "aktivitas" : self.test_activity.id,
        }, follow=True)
        self.assertContains(response, "indi")

    def test_notValid_POST(self):
        response = self.client.post(self.activity_url, {
            "member" : "indi",
            "aktivitas" : "main",
        }, follow=True)
        self.assertContains(response, "Input tidak sesuai")

    def test_member_DELETE(self):
        response = self.client.get(self.test_member_delete_url, follow=True)
        # print(response.content)
        self.assertContains(response, "berhasil dihapus")

    def test_activity_DELETE(self):
        response = self.client.get(self.test_delete_url, follow=True)
        self.assertContains(response, "berhasil dihapus")
